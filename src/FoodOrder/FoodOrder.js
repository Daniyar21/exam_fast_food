import React from 'react';
import './FoodOrder.css';

const FoodOrder = props => {
    return (
        <div className='food-order'>
            <p>{props.name}</p>
            <p>x {props.count}</p>
            <p>{props.price} som</p>
            <button onClick={props.onRemove}>X</button>
        </div>
    );
};

export default FoodOrder;