import React from 'react';
import './FastFood.css';
const FastFood = props => {
    return (
        <div className='item'>
            <img src={props.image} onClick={props.onIconClick} alt='meals'/>
            <h3>Title: {props.name}</h3>
            <p>Price: {props.price}</p>
        </div>
    );
};

export default FastFood;