import './App.css';
import shaurma from './assets/shaurma.jpeg';
import  hamburger from './assets/humburger.jpg';
import sausage from './assets/sausage.png';
import fries from './assets/fries.jpg';
import water from './assets/water.jpg';
import coca from './assets/cocacola.jpeg';
import tea from './assets/hot_tea.jpg';
import {useState} from "react";
import FastFood from "./FastFood/FastFood";
import FoodOrder from "./FoodOrder/FoodOrder";


const App = () => {
const [Foods, setFoods]=useState( [
    {id: 1, name: 'Shaurma', image: shaurma, price: 120, count: 0},
    {id: 2, name: 'Hamburger', image: hamburger, price: 150, count: 0},
    {id: 3, name: 'Sausage', image: sausage, price: 100, count: 0},
    {id: 4, name: 'Fries', image: fries, price: 130, count: 0},
    {id: 5, name: 'Water', image: water, price: 30, count: 0},
    {id: 6, name: 'Coca', image: coca, price: 70, count: 0},
    {id: 7, name: 'Tea', image: tea, price: 40, count: 0},
  ]
)

  const iconClick = id=>{
    console.log('click',id);
    setFoods(Foods.map(f=>{
        if(f.id === id){
            return{
                ...f,
                count: f.count+1,
            }
        }
        return f;
    }))
  }

  const removeOrder = (id)=>{
      setFoods(Foods.map(f=>{
          if(f.id === id){
              return{
                  ...f,
                  count: 0,
              }
          }
          return f;
      }))

  }

    const orders = Foods.filter(f=>f.count>0);

    const orderFood = orders.map(f=>(
        <FoodOrder
            key={f.id}
            name={f.name}
            price={f.price}
            count={f.count}
            onRemove = {()=>removeOrder(f.id)}
        />
    ))

    const totalPrice = orders.reduce((acc,f)=>{
        acc+=f.count*f.price;
        return acc;
    },0)

  const foods = Foods.map(f=>(
      <FastFood
          name ={f.name}
          image = {f.image}
          price = {f.price}
          onIconClick = {()=>{iconClick(f.id)}}
          key={f.id}

      />
  ))

  return(
      <div className="App">
          <div className='main-block'>
              <div className="order">
                  <p>Order Details</p>
                      {orderFood}
                 <p>Total price: {totalPrice}</p>
              </div>
              <div className="items">
                  <p>Add items</p>
                  <div className="foods">
                      {foods}
                  </div>
              </div>
          </div>

      </div>
  );
}

export default App;
